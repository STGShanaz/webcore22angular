import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/Forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome'
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastrModule } from "ngx-toastr";

 
import { AppComponent } from './app.component';
import { ParkingsComponent } from './parkings/parkings.component';
import { ParkingComponent } from './parkings/parking/parking.component';
import { ParkingInfoService } from './shared/parking-info.service';
import { ParkingListComponent } from './parkings/parking-list/parking-list.component';
import { SchedulingComponent } from './parkings/scheduling/scheduling.component';
import { ScheduleListComponent } from './parkings/schedule-list/schedule-list.component';
import { ParkingScheduleComponent } from './parkings/parking-schedule/parking-schedule.component';
import { ScheduleInfoService } from './shared/schedule-info.service';
import { HeaderComponent } from './header/header.component';

//const appRoutes: Routes = [
//  { path: 'parkings', component: ParkingsComponent }
//];

@NgModule({
  declarations: [
    AppComponent,
    ParkingsComponent,
    ParkingComponent,
    ParkingListComponent,
    SchedulingComponent,
    ScheduleListComponent,
    ParkingScheduleComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    AngularFontAwesomeModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [ParkingInfoService, ScheduleInfoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
