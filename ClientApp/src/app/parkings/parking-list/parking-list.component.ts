import { Component, OnInit } from '@angular/core';
import { ParkingInfoService } from '../../shared/parking-info.service';
import { Parking } from '../../shared/parking.model';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-parking-list',
  templateUrl: './parking-list.component.html',
  styleUrls: []
})
export class ParkingListComponent implements OnInit {

  constructor(private parkingService: ParkingInfoService, private toastr: ToastrService) { }

  ngOnInit() {
    this.parkingService.refreshParkingList();
  }

  populatePark(park: Parking) {
    this.parkingService.formData = (<any>Object).assign({}, park);
  }

  onDelete(parkingId) {
    if (confirm("Are you sure you want to delete Parking?")) {
      this.parkingService.deleteParking(parkingId).subscribe(
        res => {
          this.parkingService.refreshParkingList();
          this.toastr.warning('Deleted successfully', 'Deleting Parking');
        },
        err => {
          console.log(err)
        }
      )
    }

  }
}
