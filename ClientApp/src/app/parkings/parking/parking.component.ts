import { Component, OnInit } from '@angular/core';
import { ParkingInfoService } from '../../shared/parking-info.service';
import { NgForm } from '@angular/Forms';
import { ToastrService } from 'ngx-toastr';

 

@Component({
  selector: 'app-parking',
  templateUrl: './parking.component.html',
  styleUrls: []
})
export class ParkingComponent implements OnInit {

  constructor(private parkingService: ParkingInfoService, private toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.parkingService.formData = {
      parkingId : 0,
      parkingName: '',
      address: '',
      city: '',
      state: '',
      zipCode:''

    }
  }

  onSubmit(form: NgForm) {
    if (this.parkingService.formData.parkingId == 0)
      this.insertPark(form);
    else
      this.updatePark(form);
    
  }

  insertPark(form: NgForm) {
    this.parkingService.postParking().subscribe(
      resu => {
        this.resetForm(form);
        this.toastr.success('Submitted successfully', 'Adding New Parking');
        this.parkingService.refreshParkingList();
      },
      err => { console.log(err); }
    )
  }

  updatePark(form: NgForm) {
    this.parkingService.putParking().subscribe(
      resu => {
        this.resetForm(form);
        this.toastr.info('Submitted successfully', 'Updating Parking');
        this.parkingService.refreshParkingList();
      },
      err => { console.log(err); }
    )
  }

}

 
