import { Component, OnInit } from '@angular/core';
import { ScheduleInfoService } from '../../shared/schedule-info.service';
import { Schedule} from '../../shared/schedule.model';
import { ToastrService } from 'ngx-toastr';
 

@Component({
  selector: 'app-schedule-list',
  templateUrl: './schedule-list.component.html',
  styleUrls: []
})
export class ScheduleListComponent implements OnInit {


  constructor(private scheduleService: ScheduleInfoService, private toastr: ToastrService) { }

  ngOnInit() {
    this.scheduleService.refreshScheduleList();
  }

  populateSchedule(schedule: Schedule) {
    
    let curStYear = new Date(Date.parse(schedule.startDate)).getFullYear();
    let curStMonth = new Date(Date.parse(schedule.startDate)).getMonth() + 1;
    let curStDay = new Date(Date.parse(schedule.startDate)).getDate();
    let curEdYear = new Date(Date.parse(schedule.endDate)).getFullYear();
    let curEdMonth = new Date(Date.parse(schedule.endDate)).getMonth() + 1;
    let curEdDay = new Date(Date.parse(schedule.endDate)).getDate();
    this.scheduleService.formData = (<any>Object).assign({}, schedule);

    this.scheduleService.formData.startDate = curStYear.toString() + '-' + ("0" + curStMonth.toString()).slice(-2) + '-' + ("0" + curStDay.toString()).slice(-2);
    this.scheduleService.formData.endDate = curEdYear.toString() + '-' + ("0" + curEdMonth.toString()).slice(-2) + '-' + ("0" + curEdDay.toString()).slice(-2);

  }
  onDelete(scheduleId: number) {
    if (confirm("Are you sure you want to delete the schedule?")) {
      this.scheduleService.deleteSchedule(scheduleId).subscribe(
        res => {
          this.scheduleService.refreshScheduleList();
          this.toastr.warning("Deleted Successfully", "Deleting Schedule");
        },
        err => {
          console.log(err);
        }
      );
    }
  }
}
