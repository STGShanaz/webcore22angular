import { Component, OnInit } from '@angular/core';
import { ScheduleInfoService } from '../../shared/schedule-info.service';
import { NgForm } from '@angular/Forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-scheduling',
  templateUrl: './scheduling.component.html',
  styleUrls: []
})
export class SchedulingComponent implements OnInit {

  constructor(private scheduleService: ScheduleInfoService, private toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.scheduleService.formData = {
      scheduleId: 0,
      startDate: '',
      endDate: '',
      startTime: '',
      endTime: '',
      amount: 0

    }
  }
  onSubmit(form: NgForm) {
    if (this.scheduleService.formData.scheduleId == 0)
      this.insertSchedule(form);
    else
      this.updateSchedule(form);

  }

  insertSchedule(form: NgForm) {
    this.scheduleService.postSchedule().subscribe(
      res => {
        this.resetForm(form);
        this.toastr.success("Submitted Successfully", "Adding New Schedule");
        this.scheduleService.refreshScheduleList();
      },
      err => {
        console.log(err)
      }
    )
  }

  updateSchedule(form: NgForm) {
    this.scheduleService.putSchedule().subscribe(
      res => {
        this.resetForm(form);
        this.toastr.success("Updated Successfully", "Updating Schedule");
        this.scheduleService.refreshScheduleList();
      },
      err => {
        console.log(err)
      }
    )
  }

  
}
