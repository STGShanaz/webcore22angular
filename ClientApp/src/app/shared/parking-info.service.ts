import { Injectable } from '@angular/core';
import { Parking } from './parking.model';
import {HttpClient } from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class ParkingInfoService {
  formData: Parking;
  readonly rootUrl = 'http://localhost:58369/api';
  list: Parking[];
 
  constructor(private http:HttpClient) { }

  postParking() {
    return this.http.post(this.rootUrl + '/Parkings', this.formData);
  }

  putParking() {
    return this.http.put(this.rootUrl + '/Parkings/' +  this.formData.parkingId, this.formData);
  }

  deleteParking(parkingId:number) {
    return this.http.delete(this.rootUrl + '/Parkings/' + parkingId);
  }
  refreshParkingList() {
    this.http.get(this.rootUrl + '/Parkings')
      .toPromise()
      .then(res => this.list = res as Parking[]);
  }

}
