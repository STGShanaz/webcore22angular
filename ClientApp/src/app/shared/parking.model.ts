export class Parking {
  parkingId: number;
  parkingName: string;
  address: string;
  city: string;
  state: string;
  zipCode: string;
}
