import { Injectable } from '@angular/core';
import { Schedule } from './schedule.model';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ScheduleInfoService {
  formData: Schedule;
  list: Schedule[];
  readonly rootUrl = 'http://localhost:58369/api';

  constructor(private http: HttpClient) { }

  postSchedule() {
    return this.http.post(this.rootUrl + '/Schedules', this.formData);
  }

  putSchedule() {
    return this.http.put(this.rootUrl + '/Schedules/' + this.formData.scheduleId, this.formData);
  }

  deleteSchedule(scheduleId: number) {
    return this.http.delete(this.rootUrl + '/Schedules/' + scheduleId);
  }

  refreshScheduleList() {
    this.http.get(this.rootUrl + '/Schedules')
      .toPromise()
      .then(res => this.list = res as Schedule[]);
  }

}
