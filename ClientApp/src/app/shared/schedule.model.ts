export class Schedule {
  scheduleId: number;
  startDate: string;
  endDate: string;
  startTime: string;
  endTime: string;
  amount: number;
}
