﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebCore22Angular.Models;

namespace WebCore22Angular.Data
{
    public class ParkingDbContext : DbContext
    {
        public ParkingDbContext(DbContextOptions option) : base(option)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //adding Fluent configuration for Many-to-Many relationship...
            modelBuilder.Entity<ParkingSchedule>()
                        .HasKey(pl => new { pl.ParkingId, pl.ScheduleId });
            modelBuilder.Entity<ParkingSchedule>()
                        .HasOne(p => p.Parking)
                        .WithMany(pl => pl.ParkingSchedules)
                        .HasForeignKey(p => p.ParkingId);
            modelBuilder.Entity<ParkingSchedule>()
                        .HasOne(s => s.Schedule)
                        .WithMany(pl => pl.ParkingSchedules)
                        .HasForeignKey(s => s.ScheduleId);
                        
        }

        public DbSet<Parking>Parkings { get; set; }
        public DbSet<ParkingSchedule> ParkingSchedules { get; set; }
        public DbSet<Schedule> Schedules { get; set; }

    }
}
