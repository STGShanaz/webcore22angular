﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebCore22Angular.Models
{
    public class ParkingSchedule
    {
        public int ParkingId { get; set; }
        public int ScheduleId { get; set; }
        public Parking Parking { get; set; }
        public Schedule Schedule { get; set; }

    }
}
