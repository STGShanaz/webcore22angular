﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebCore22Angular.Models
{
    public class Schedule
    {
        public int ScheduleId { get; set; }
        public DateTime StartDate { get; set; }
        public TimeSpan StartTime { get; set; }
        public DateTime EndDate { get; set; }
        public TimeSpan EndTime { get; set; }
        public decimal Amount { get; set; }
        public ICollection<ParkingSchedule> ParkingSchedules { get; set; }


    }
}
